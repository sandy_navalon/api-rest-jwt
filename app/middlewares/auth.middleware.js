//Este middleware se va a encargar de verificar que la petición trae un token
//válido y dar permiso para continuar o arrojar un error ya que el token no
//es válido.

const jwt = require('jsonwebtoken');
const httpStatusCode = require('../utils/httpStatusCode');

const isAuth = (req, res, next) => {
    //guardamos en una variable la información de la autorizacion de la cabecera
    //de la petición
    const authorization = req.headers.authorization;
    //si no existe autorizacion, no hay token y devuelve error
    if(!authorization){
        return res.json({
            status: 401,
            message: httpStatusCode[401],
            data: null
        });
    }
    //si hay token, lo "troceamos " para separar la parte bearer de la info
    //del token
    const split = authorization.split('')
    if( split.length != 2 || split[0] != 'Bearer' ){
        return res.json({
            status:400,
            message: httpStatusCode[400],
            data: null
        })
    }
    //guardamos la info del token
    const jwtString = split[1];
    try{
        //verificamos token antes de guardarlo en una var
        var token = jwt.verify(jwtString, req.app.get('secretKey'));
    }catch(err) {
        return next(err)
    };
    //creamos una var con la info  para la peticion
    const authority = {
        id: token.id,
        name: token.name
    }
    //se la asignamos al req de la peticion
    req.authority = authority
    //si todo ha funcionado, pasaremos el middleware

    next()
};

module.exports = {
    isAuth,
}