const express = require ('express');
const router = express.Router();

//importamos middleware
const {isAuth} = require('../../middlewares/auth.middleware');

//importamos funciones controlador
const {
    newPalette,
    getAllPalettes,
    getPalettesById,
    deletePaletteById,
    updatePaletteById,
    getAllPalettesByUser
} = require('../controllers/palette.controller');

//ruta CREAR paleta
router.post('/create', [isAuth], newPalette);

//ruta ACCESO a paletas
router.get('/', [isAuth], getAllPalettes);

//ruta paleta USER
router.get('/palettesbyuser', [isAuth], getAllPalettesByUser);

//ruta paleta por ID
router.get('/:paletteId', [isAuth], getPalettesById);

//ruta para BORRAR paleta
router.delete('/:paletteId', [isAuth], deletePaletteById);

//ruta para MODIFICAR paleta
router.put('/:paletteId', [isAuth], updatePaletteById);

module.exports = router;
