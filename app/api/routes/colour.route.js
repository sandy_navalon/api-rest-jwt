const express = require('express');

const router = express.Router();

//Importamos las funciones del controlador de color
const { getAllColours, getColourById } = require('../controllers/colour.controller');


//Definimos el metodo, la ruta de entrada y la función del controlador
//que se encargará de efectuar la lógica
router.get('/', getAllColours);
router.get('/:colourId', getColourById);

module.exports = router;