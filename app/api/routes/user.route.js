const express = require('express');
const router = express.Router();

//importamos controller y middleware functions
const { createUser, authenticate, logout } = require('../controllers/user.controller');
const { isAuth } = require('../../middlewares/auth.middleware');

router.post('/register', createUser);
router.post('/authenticate', authenticate);
//añadimos middleware para que solo pueda acceder si esta logueado
router.post('/logout', [isAuth], logout)
//si un usuario que no esta logueado se quiere desloguear el middleware
//se va a encargar de rechazarle y no va a permitir que se llame
//a la función logout del controlador.

module.exports = router;