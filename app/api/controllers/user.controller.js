const User = require('../models/User');

const bcrypt = require('bcrypt');

const jwt = require('jsonwebtoken');

const HTTPSTATUSCODE = require('../../utils/httpStatusCode');

// Codificamos las operaciones que se podran realizar con relacion a los usuarios
const createUser = async(req, res, next) => {
    try{
        const newUser = new User();
        newUser.name = req.body.name;
        newUser.emoji = req.body.emoji;
        newUser.email = req.body.email;
        newUser.password = req.body.password;
        newUser.favPalettes = [];

        //comprobar si el user existe antes de guardar
        const userDb = await newUser.save();

        //autenticar al user
        return res.json({
            status: 201,
            message: HTTPSTATUSCODE[201],
            data: null
        });
    }catch (err) {
        return next (err);
    }
};

const authenticate = async(req, res, next) => {
    try{
        //buscamos al user en la DB
        const userInfo = await User.findOne({email:req.body.email});
        //comparamos pass
        if(bcrypt.compareSync(req.body.password, userInfo.password)) {
            //elimina user pass
            userInfo.password = null;

            //creamos token con ID y name de user
            const token = jwt .sign(
                {
                    id: userInfo._id,
                    name: userInfo.name
                },
                req.app.get('secretKey'),
                { expiresIn: '1h' }
            );
            //devolvemos user y token
            return res.json({
                status:200,
                message: HTTPSTATUSCODE[200],
                data: { user: userInfo, token: token},
            });
        }else {
            return res.json({ 
                status: 400,
                message: HTTPSTATUSCODE[400],
                data: null
            });
        }
    }catch (err){
        return next(err);
    }
};

//LOGOUT function, iguala token a null
const logout = (req, res, next) => {
    try{
        return res.json ({
            status: 200,
            message: HTTPSTATUSCODE[200],
            token: null
        });
    }catch(err) {
        return next(err)
    }
};

module.exports = {
    createUser,
    authenticate,
    logout
}
