const Colour = require('../models/Colour');

const HTTPSTATUSCODE = require('../../utils/httpStatusCode');


//DEVOLVER TODOS LOS COLORES DE LA DB
const getAllColours = async (req, res, next) => {
    try{
        if (req.query.page) {
            //se añade paginación
            const page = parseInt(req.query.page);
            const skip = (page - 1)* 20;
            const colours = await Colour.find().skip(skip).limit(20);
            return res.json({
                status: 200,
                message: HTTPSTATUSCODE[200],
                data: { colours: colours },
            });
        }else {
            const colours = await Colour.find();
            return res.json({
                status: 200,
                message: HTTPSTATUSCODE[200],
                data: { colours: colours }
            });
        }
    }catch (err){
        return next(err);
    }
};

//BUSQUEDA POR ID
const getColourById = async (res, req, next) => {
    try{
        const { colourId }= req.params;
        const colourById = await Colour.findById(colourId);
        return res.json({
            status: 200,
            message: HTTPSTATUSCODE[200],
            data: { colours: colourById }
        });
    }catch (err){
        return next(err);
    }
};

module.exports = {
    getAllColours,
    getColourById,
}