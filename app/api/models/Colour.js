const mongoose = require ('mongoose');

const Schema = mongoose.Schema;

const ColourSchema = new Schema(
    {
        hex: { type: String, require: true },//código de color en valor hexadecimal.
        name: { type: String, require: true },
        rgb: { type: String, require: true },
    },
    { timestamps: true }

);

const Colour = mongoose.model('colours', ColourSchema);
module.exports = Colour;