//Requerimos dotenv para acceder a las variables de entorno
const dotenv = require("dotenv");
dotenv.config();

//Requerimos mongoose para comunicarnos con la DB
const mongoose = require("mongoose");

const mongoDb = process.env.MONGO_DB;

//Configuramos la función connect
const connect = async () => {
  try {
    const db = await mongoose.connect(mongoDb, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    const { name, host } = db.connection;
    console.log(`Connected with db: ${name}, in host: ${host}`);
  } catch (error) {
    console.log("Error to connect with BD", error);
  }
};

//EXPORTAMOS LA FUNCTION connect
module.exports = { connect };